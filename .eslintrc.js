module.exports = {
  extends:  require.resolve('@elementinsurance/presets/eslint/config.es'),
  parser: 'babel-eslint',
  rules: {
    'no-global-assign': ['error', { exceptions: ['fetch'] }],
    'prefer-promise-reject-errors': 'off',
  },
}
