import React from 'react'
import PropTypes from 'prop-types'

import { getComponents } from '@chalklinesports/utils'

import * as defaultComponents from './components'

import './footer.scss'

const Footer = ({
  menu,
  className,
  children,
  overrides,
  showLeft,
  showRight,
  ...props
}) => {
  const {
    Root: { component: Root, props: rootProps },
    Block: { component: Block, props: blockProps },
    Menu: { component: Menu, props: menuProps },
    MenuItem: { component: MenuItem, props: menuItemProps },
    Logo: { component: Logo, props: logoProps }
  } = getComponents(defaultComponents, overrides)

  const renderMenuItems = ({ ...itemProps }, idx) => (
    <MenuItem key={idx} {...itemProps} {...menuItemProps} />
  )

  const renderMenu = ({ title, items }, idx) => {
    return (
      <Menu key={`${title}-${idx}`} items={items} title={title} {...menuProps}>
        {({ items }) => items.map(renderMenuItems)}
      </Menu>
    )
  }

  return (
    <Root className={className} {...rootProps} {...props}>
      {showLeft && (
        <Block type={showRight ? 'left' : ''} {...blockProps}>
          <Logo {...logoProps} />
        </Block>
      )}
      {showRight && (
        <Block type={showLeft ? 'right' : ''} {...blockProps}>
          {menu.map(renderMenu)}
        </Block>
      )}
      {children}
    </Root>
  )
}

Footer.defaultProps = {
  showRight: true,
  showLeft: true,
  menu: [],
  overrides: {}
}

Footer.propTypes = {
  showRight: PropTypes.bool,
  showLeft: PropTypes.bool,
  menu: PropTypes.array
}

export default Footer
