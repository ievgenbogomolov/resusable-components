import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './menu.scss'

const Menu = ({ children, items, title, direction, className, ...props }) => {
  return (
    <div className={cn('footer-menu', direction, className)}>
      <h4>{title}</h4>
      <ul className={direction} {...props}>{children({ items })}</ul>
    </div>
  )
}

export const menuProps = {
  direction: PropTypes.oneOf(['column', 'row']),
  className: PropTypes.string,
  items: PropTypes.array
}

Menu.defaultProps = {
  items: [],
  direction: 'column'
}

Menu.propTypes = menuProps

export default Menu
