import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './root.scss'

const Root = ({ children, className, ...props }) => {
  return (
    <footer className={cn('footer', className)} {...props}>
      {children}
    </footer>
  )
}

export const rootProps = {
  className: PropTypes.string
}

Root.propTypes = rootProps

export default Root
