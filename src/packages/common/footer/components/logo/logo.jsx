import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './logo.scss'

const Logo = ({ children, className, src, onClick, name, size, ...props }) => {
  return (
    <div className={cn('footer-logo', className, size)}>
      <img
        className={cn('image', size)}
        src={src}
        alt={name}
        onClick={onClick}
        {...props}
      />
      {children}
    </div>
  )
}

export const logoProps = {
  size: PropTypes.oneOf(['s', 'm', 'l']),
  className: PropTypes.string,
  src: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func
}

Logo.defaultProps = {
  size: 'm'
}

Logo.propTypes = logoProps

export default Logo
