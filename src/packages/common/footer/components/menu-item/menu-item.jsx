import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './menu-item.scss'

const MenuItem = ({ children, name, href, className, ...props }) => {
  return (
    <li className={cn('footer-menu-item', className)} {...props}>
      <a href={href}>{name}</a>
      {children}
    </li>
  )
}

export const menuItemProps = {
  className: PropTypes.string
}

MenuItem.propTypes = menuItemProps

export default MenuItem
