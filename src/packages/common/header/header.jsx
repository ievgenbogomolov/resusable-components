import React from 'react'
import PropTypes from 'prop-types'
import { getComponents } from '@chalklinesports/utils'

import * as defaultComponents from './components'

import './header.scss'

const Header = ({
  menu,
  actions,
  className,
  children,
  overrides,
  showLeft,
  showRight,
  showMiddle,
  ...props
}) => {
  const {
    Root: { component: Root, props: rootProps },
    Buttons: { component: Buttons, props: buttonsProps },
    Button: { component: Button, props: buttonProps },
    Block: { component: Block, props: blockProps },
    Menu: { component: Menu, props: menuProps },
    MenuItem: { component: MenuItem, props: menuItemProps },
    Logo: { component: Logo, props: logoProps }
  } = getComponents(defaultComponents, overrides)

  const renderMenuItems = ({ ...itemProps }, idx) => (
    <MenuItem key={idx} {...itemProps} {...menuItemProps} />
  )

  const renderActionButtons = ({ name, ...actionProps }, idx) => {
    return (
      <Button key={`${name}-${idx}`} {...actionProps} {...buttonProps}>
        {name}
      </Button>
    )
  }

  return (
    <Root className={className} {...rootProps} {...props}>
      {showLeft && (
        <Block type={showRight ? 'left' : ''} {...blockProps}>
          <Logo {...logoProps} />
        </Block>
      )}
      {showMiddle && (
        <Block type="middle" {...blockProps}>
          <Menu items={menu} {...menuProps}>
            {({ items }) => items.map(renderMenuItems)}
          </Menu>
        </Block>
      )}
      {showLeft && (
        <Block type={showLeft ? 'right' : ''} {...blockProps}>
          <Buttons actions={actions} {...buttonsProps}>
            {({ actions }) => actions.map(renderActionButtons)}
          </Buttons>
        </Block>
      )}
      {children}
    </Root>
  )
}

Header.defaultProps = {
  showRight: true,
  showLeft: true,
  showMiddle: true,
  menu: [],
  actions: [],
  overrides: {}
}

Header.propTypes = {
  showRight: PropTypes.bool,
  showLeft: PropTypes.bool,
  showMiddle: PropTypes.bool,
  menu: PropTypes.array,
  actions: PropTypes.array
}

export default Header
