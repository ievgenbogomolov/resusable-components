import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './buttons.scss'

const Buttons = ({ children, actions, title, className, ...props }) => {
  return (
    <div className={cn('header-buttons', className)}>
      {children({ actions })}
    </div>
  )
}

export const buttonsProps = {
  className: PropTypes.string,
  actions: PropTypes.array
}

Buttons.defaultProps = {
  actions: []
}

Buttons.propTypes = buttonsProps

export default Buttons
