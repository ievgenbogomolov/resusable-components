import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './menu-item.scss'

const MenuItem = ({ children, name, path, className, ...props }) => {
  return (
    <li className={cn('header-menu-item', className)} {...props}>
      <a href={path}>{name}</a>
      {children}
    </li>
  )
}

export const menuItemProps = {
  className: PropTypes.string
}

MenuItem.propTypes = menuItemProps

export default MenuItem
