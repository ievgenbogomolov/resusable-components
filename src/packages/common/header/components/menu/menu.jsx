import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './menu.scss'

const Menu = ({ children, items, className, ...props }) => {
  return (
    <div className={cn('header-menu', className)}>
      <ul {...props}>{children({ items })}</ul>
    </div>
  )
}

export const menuProps = {
  className: PropTypes.string,
  items: PropTypes.array
}

Menu.defaultProps = {
  items: []
}

Menu.propTypes = menuProps

export default Menu
