import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './block.scss'

const Block = ({ children, className, fullWidth, type, ...props }) => {
  return (
    <div
      className={cn('header-block', className, type, {
        fullWidth: !!fullWidth
      })}
      {...props}>
      {children}
    </div>
  )
}

export const blockProps = {
  className: PropTypes.string,
  fullWidth: PropTypes.bool
}

Block.propTypes = blockProps

export default Block
