import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './root.scss'

const Root = ({ children, className, ...props }) => {
  return (
    <header className={cn('header', className)} {...props}>
      {children}
    </header>
  )
}

export const rootProps = {
  className: PropTypes.string
}

Root.propTypes = rootProps

export default Root
