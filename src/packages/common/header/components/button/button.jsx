import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import './button.scss'

const Button = ({
  component: Component,
  children,
  color,
  className,
  ...props
}) => {
  return (
    <Component className={cn('header-button', color, className)} {...props}>
      {children}
    </Component>
  )
}

export const buttonProps = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  className: PropTypes.string,
  onClick: PropTypes.func,
  to: PropTypes.string,
  color: PropTypes.oneOf(['primary', 'secondary'])
}

Button.defaultProps = {
  color: 'primary',
  component: 'button'
}

Button.propTypes = buttonProps

export default Button
