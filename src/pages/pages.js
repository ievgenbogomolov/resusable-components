import React, { useCallback } from 'react'
import { Switch, Route } from 'react-router-dom'

import routes from './pages.config'

const Pages = ({ ...props }) => {
  const getRoutes = useCallback((route, idx) => {
    const { component: Component } = route
    return (
      <Route
        key={`rout-${idx}-${route.name}`}
        exact
        path={route.path}
        render={routerProps => <Component {...routerProps} pages={routes} />}
      />
    )
  }, [])

  const navRoutes = routes.map(getRoutes)

  return <Switch>{navRoutes}</Switch>
}

export default Pages
