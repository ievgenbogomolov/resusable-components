import React from 'react'
import Footer from './footer'
import Header from './header'

import './third.scss'

const ThirdPage = ({ pages, ...props }) => {
  const links = pages.map(({ path, name }) => ({
    path,
    name
  }))

  return (
    <div className="page">
      <Header menu={links} />
      <div className="content">
        <h2>Here we are using Full component replacement for Footer</h2>
        <h4>
          and Modification for Header with already exist reusable components
        </h4>
        <p>Check source code of implementation</p>
      </div>
      <Footer />
    </div>
  )
}

export default ThirdPage
