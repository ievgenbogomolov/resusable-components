import React from 'react'
import { Footer } from '@chalklinesports/common'

import { Logo } from './components'

import './footer.scss'

const MainFooter = ({ menu, ...props }) => {
  const overrides = {
    Logo: {
      component: Logo
    },
    Block: {
      props: {
        className: 'footer1-block'
      }
    }
  }

  return <Footer menu={menu} className="footer3" overrides={overrides} />
}

export default MainFooter
