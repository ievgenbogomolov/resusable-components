import React from 'react'
import cn from 'classnames'
import PropTypes from 'prop-types'

import LogoSvg from './img/logo.svg'

import './logo.scss'

const LogoOverride = ({
  children,
  className,
  src,
  onClick,
  name,
  ...props
}) => {
  return (
    <div className={cn('footer3-logo', className)}>
      <img className="logo" src={LogoSvg} alt={name} {...props}>
        {children}
      </img>
      <h3>Full component logo replacement</h3>
    </div>
  )
}

export const logoProps = {
  className: PropTypes.string,
  src: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func
}

LogoOverride.defaultProps = {
  size: 'm'
}

LogoOverride.propTypes = logoProps

export default LogoOverride
