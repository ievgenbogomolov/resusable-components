import React from 'react'
import { Header } from '@chalklinesports/common'

import { Logo } from './components'

import './header.scss'

const MainHeader = ({ menu, ...props }) => {
  const overrides = {
    Logo: {
      component: Logo,
      props: {
        size: 's'
      }
    },
    MenuItem: {
      props: {
        className: 'header3-item'
      }
    }
  }

  return <Header menu={menu} className="header3" overrides={overrides} />
}

export default MainHeader
