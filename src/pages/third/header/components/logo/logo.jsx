import React from 'react'
import PropTypes from 'prop-types'
import { Logo } from '@chalklinesports/common/header/components'

import LogoSvg from './img/logo.svg'

import './logo.scss'

const LogoExtended = ({
  children,
  className,
  src,
  onClick,
  name,
  ...props
}) => {
  return (
    <Logo className="header3-logo" src={LogoSvg} name="Some logo" {...props}>
      <h3 className="text">Extended logo from original logo component</h3>
    </Logo>
  )
}

export const logoProps = {
  className: PropTypes.string,
  src: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func
}

LogoExtended.defaultProps = {
  size: 'm'
}

LogoExtended.propTypes = logoProps

export default LogoExtended
