import { withRouter } from 'react-router-dom'

import Pages from './pages'

export default withRouter(Pages)
