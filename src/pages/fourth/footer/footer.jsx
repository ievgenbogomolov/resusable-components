import React from 'react'
import PropTypes from 'prop-types'
import {
  Root,
  Block,
  MenuItem,
  Menu,
  Logo
} from '@chalklinesports/common/footer/components'

import LogoSvg from './img/logo.svg'

import './footer.scss'

const MainFooter = ({ navigation, sublinks, ...props }) => {
  const renderMenuItems = ({ ...itemProps }, idx) => (
    <MenuItem className="menu-item" key={idx} {...itemProps} />
  )

  return (
    <Root className="footer4" {...props}>
      <Block className="row" fullWidth>
        <Block type="left">
          <Logo className="logo center" src={LogoSvg} name="Some logo">
            <h3>Some info about brand</h3>
          </Logo>
        </Block>
        <Block type="right">
          <p className="text">
            Absolutely new Footer component base on Components from original
            Footer
          </p>
        </Block>
      </Block>
      <Block className="row" fullWidth>
        <Block className="row center" type="left">
          <Menu className="menu" items={navigation} title="Navigation">
            {({ items }) => items.map(renderMenuItems)}
          </Menu>
          <Menu className="menu" items={sublinks} title="Other">
            {({ items }) => items.map(renderMenuItems)}
          </Menu>
        </Block>
        <Block className="row" type="right">
          <ul className="list">
            <li>
              <a className="link" href="tel:+380 066 123 45 67">
                +38 066 123 45 67
              </a>
            </li>
            <li>
              <a className="link" href="tel:+38 050 123 45 67">
                +38 050 123 45 67
              </a>
            </li>
            <li>
              <a className="link" href="tel:+38 072 123 45 67">
                +38 072 123 45 67
              </a>
            </li>
          </ul>
          <ul className="list">
            <li>
              <a className="link" href="maito:some.mail@gmail.com">
                some.mail@gmail.com
              </a>
            </li>
            <li>
              <a className="link" href="maito:test.mail@gmail.com">
                test.mail@gmail.com
              </a>
            </li>
            <li>
              <a className="link" href="maito:other.mail@gmail.com">
                other.mail@gmail.com
              </a>
            </li>
          </ul>
        </Block>
      </Block>
    </Root>
  )
}

MainFooter.propTypes = {
  navigation: PropTypes.array
}

export default MainFooter
