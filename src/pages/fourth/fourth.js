import React from 'react'
import PropTypes from 'prop-types'

import Header from './header'
import Footer from './footer'

const navigation = [
  {
    href: '/torprämie',
    name: 'Torprämie'
  },
  {
    href: '/marktgarantie',
    name: 'Marktgarantie'
  },
  {
    href: '/frustpauschale',
    name: 'Frustpauschale'
  }
]

const sublinks = [
  {
    href: '/kontakt',
    name: 'Kontakt'
  },
  {
    href: '/uns',
    name: 'Über Uns'
  },
  {
    href: '/faq',
    name: 'FAQ'
  }
]

const Fourth = ({ pages, ...props }) => {
  const links = pages.map(({ path, name }) => ({
    path,
    name
  }))

  return (
    <div className="page">
      <Header menu={links} />
      <div className="content">
        <h2>Here we are using absolutely custom Footer</h2>
        <h4>From already exist reusable components</h4>
        <p>Check source code of implementation</p>
      </div>
      <Footer navigation={navigation} sublinks={sublinks} />
    </div>
  )
}

Fourth.propTypes = {
  pages: PropTypes.array
}

export default Fourth
