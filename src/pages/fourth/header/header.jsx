import React from 'react'
import { Header } from '@chalklinesports/common'

import LogoSvg from './img/logo.svg'

import './header.scss'

const MainHeader = ({ menu, ...props }) => {
  const overrides = {
    Logo: {
      props: {
        src: LogoSvg,
        size: 's'
      }
    },
    MenuItem: {
      props: {
        className: 'item'
      }
    }
  }

  return <Header menu={menu} className="header4" overrides={overrides} />
}

export default MainHeader
