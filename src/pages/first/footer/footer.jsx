import React from 'react'
import { Footer } from '@chalklinesports/common'

import LogoPng from './img/logo.png'

import './footer.scss'

const MainFooter = ({ menu, ...props }) => {
  const overrides = {
    Logo: {
      props: {
        src: LogoPng
      }
    },
    Block: {
      props: {
        className: 'footer1-block'
      }
    }
  }

  return <Footer menu={menu} className="footer1" overrides={overrides} />
}

export default MainFooter
