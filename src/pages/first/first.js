import React from 'react'

import Footer from './footer'
import Header from './header'

import './first.scss'

const menu = [
  {
    title: 'Fanvorteile',
    items: [
      {
        href: '/torprämie',
        name: 'Torprämie'
      },
      {
        href: '/marktgarantie',
        name: 'Marktgarantie'
      }
    ]
  },
  {
    title: 'Rechtliches',
    items: [
      {
        href: '/kontakt',
        name: 'Kontakt'
      },
      {
        href: '/uns',
        name: 'Über Uns'
      }
    ]
  }
]

const FirstPage = ({ pages, ...props }) => {
  const links = pages.map(({ path, name }) => ({
    path,
    name
  }))
  return (
    <div className="page">
      <Header menu={links} />
      <div className="content">
        <h2>
          Here we are using customization with overrides for Header and Footer
        </h2>
        <p>Check source code of implementation</p>
      </div>
      <Footer menu={menu} />
    </div>
  )
}

export default FirstPage
