import React from 'react'
import { Link } from 'react-router-dom'
import { Header } from '@chalklinesports/common'

import LogoPng from './img/logo.png'

import './header.scss'

const actions = [
  {
    name: 'login',
    component: Link,
    to: '/login'
  },
  {
    name: 'register',
    component: Link,
    to: '/register'
  }
]

const MainHeader = ({ menu, ...props }) => {
  const overrides = {
    Logo: {
      props: {
        src: LogoPng
      }
    }
  }

  return (
    <Header
      menu={menu}
      className="header1"
      overrides={overrides}
      actions={actions}
    />
  )
}

export default MainHeader
