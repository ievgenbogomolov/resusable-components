import First from './first'
import Second from './second'
import Third from './third'
import Fourth from './fourth'

const routes = [
  {
    path: '/',
    name: 'first',
    permission: ['guest'],
    component: First
  },
  {
    path: '/second',
    name: 'Second',
    permission: ['guest'],
    component: Second
  },
  {
    path: '/third',
    name: 'Third',
    permission: ['guest'],
    component: Third
  },
  {
    path: '/fourth',
    name: 'Fourth',
    permission: ['guest'],
    component: Fourth
  }
]

export default routes
