import React from 'react'
import { Footer } from '@chalklinesports/common'

import './footer.scss'

const MainFooter = ({ menu, ...props }) => {
  const overrides = {
    Block: {
      props: {
        fullWidth: true,
        className: 'footer2-block'
      }
    },
    Menu: {
      props: {
        className: 'footer2-menu',
        direction: 'row'
      }
    },
    MenuItem: {
      props: {
        className: 'footer2-menu-item'
      }
    }
  }

  return (
    <Footer
      menu={menu}
      className="footer2"
      overrides={overrides}
      showLeft={false}>
      <p className="text">
        1 Vermittelt durch die Volkswagen Versicherungsdienst GmbH,
        Versicherungsleistungen durch die ELEMENT Insurance AG, Hardenbergstraße
        32, 10623 Berlin.
      </p>
      <p className="footer2-copyright">© Volkswagen Financial Services 2019</p>
    </Footer>
  )
}

export default MainFooter
