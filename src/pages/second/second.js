import React from 'react'
import { Header } from '@chalklinesports/common'
import Footer from './footer'

import './second.scss'

const menu = [
  {
    title: '',
    items: [
      {
        href: '/datenschutzinformationen',
        name: 'Datenschutzinformationen '
      },
      {
        href: '/impressum',
        name: 'Impressum'
      },
      {
        href: '/erstinformation',
        name: 'Erstinformation'
      }
    ]
  }
]

const SecondPage = ({ pages, ...props }) => {
  const links = pages.map(({ path, name }) => ({
    path,
    name
  }))
  return (
    <div className="page">
      <Header menu={links} />
      <div className="content">
        <h2>Here we are using customization with overrides for Footer</h2>
        <p>Check source code of implementation</p>
      </div>
      <Footer menu={menu} />
    </div>
  )
}

export default SecondPage
