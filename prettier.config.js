const defaultPrettierConfig = require('@elementinsurance/presets/prettier');
module.exports = {
    ...defaultPrettierConfig,
    parser: 'babel'
}
